
import apolloserver from 'apollo-server';
const { gql } = apolloserver;

export default gql`
  # Main element of the schema that define a feature for show in front and their info
  type Feature {
    id: ID
    name: String
    status: Int
    components: [CompFeature]
    configurationFeature: ConfigFeature
  }

  # Element that compose a feature and have information about their conditions and their status
  type CompFeature {
    compX: ReferenceComponent
    compY: ReferenceComponent
    compZ: ReferenceComponent
    compDiameter: ReferenceComponent
  }

  # Element that indicate the information of a reference in a component
  type ReferenceComponent {
    dimension: String
    dev: String
    devOut: String
    validation: Int
  }

  # Element that have the configuration and other infor for a feature
  type ConfigFeature {
    nameFeature: String
    numberComponents: Int
  }

  # Declaration of queries on Graph.
  # getFeatures: response with a list of random Features
  # getFeaturesAutoUp: response with a list of random Features and initialice a function that perodically generate changes on features and report it with pubsub
  type Query {
    getFeatures: [Feature],
    getFeaturesAutoUp: [Feature]
  }

  # Declaration of subscriptions on Graph.
  # changeFeature: When is called this subcription, report a updated Feature to the original method caller on front
  type Subscription {
    changeFeature: Feature
  }
`;
