//  Implementation of code that manage the featureResolvers

let controllInterval = undefined;

// This funtion generate a random change on the active features and send it to the subscription.
const updaterFeaturesCicle = (listFeaturesWork, context) => {
    const indexWork = parseInt((Math.random() * listFeaturesWork.length).toString(), 10);
    const featureWork = listFeaturesWork[indexWork];
    const componenets_size = featureWork.components.length;
    const listComponents = ([...Array(componenets_size).keys()]).map(builderComponent);

    featureWork.components = listComponents;
    featureWork.status = managerLogicStatus(listComponents);

    context.pubsub.publish('changeFeature', {
      changeFeature: featureWork,
    });
}

// This function generate a random number of Features and return it
const builderRandomFeature = (parent, args, context) => {
  const numberFeature = (parseInt((Math.random() * 4).toString(), 10) + 1);
  return ([...Array(numberFeature).keys()]).map(implementationOfFeature);
}

const implementationOfFeature = (useFeature) => {
  const nameFeature = ('Feature_' + useFeature);
  const workConfig = builderConfiguration(nameFeature);
  const listComponents = ([...Array(workConfig.numberComponents).keys()]).map(builderComponent);
  return {
            id: useFeature,
            name: nameFeature,
            components: listComponents,
            configurationFeature: workConfig,
            status: managerLogicStatus(listComponents)
          }
}

// manage a cal of Features generation, return this list of features and initiate a periodical function for update this list and report it to the subscription
const builderRandomFeatureAuto = (parent, args, context) => {
  const listFeaturesWork = builderRandomFeature(parent, args, context)
  if(controllInterval) {
    clearInterval(controllInterval);
  }
  controllInterval = setInterval(function(){
    updaterFeaturesCicle(listFeaturesWork, context);
  }, 5000);
  return listFeaturesWork;
}

const builderReferenceInfo = (dimensionRef) => {
  const workDev = (Math.random()).toFixed(3);
  const workDevOut = ((workDev * (Math.random() + 1))).toFixed(3);
  return {
            dimension: dimensionRef,
            dev: workDev,
            devOut: workDevOut,
            validation: (workDevOut > 1.7 ? 2
                        : workDevOut > 0.7 ? 1
                        : 0)
          };
}

const builderComponent = () => {
  return {
    compX: builderReferenceInfo('X'),
    compY: builderReferenceInfo('Y'),
    compZ: builderReferenceInfo('Z'),
    compDiameter: builderReferenceInfo('Diameter')
  };
}

const builderConfiguration = (nameCurrentFeature) => {
  return {
    nameFeature: nameCurrentFeature,
    numberComponents: (parseInt((Math.random() * 5).toString(), 10) + 1)
  }
}

const managerLogicStatus = (listComponents) => {
  let computationFail = 0;
  let computationWarn = 0;
  for (const nodeComponent of listComponents) {
    if (nodeComponent.compX.validation === 1) {
      computationWarn++;
    } else if (nodeComponent.compX.validation === 2) {
      computationFail++;
    }

    if (nodeComponent.compY.validation === 1) {
      computationWarn++;
    } else if (nodeComponent.compY.validation === 2) {
      computationFail++;
    }

    if (nodeComponent.compZ.validation === 1) {
      computationWarn++;
    } else if (nodeComponent.compZ.validation === 2) {
      computationFail++;
    }

    if (nodeComponent.compDiameter.validation === 1) {
      computationWarn++;
    } else if (nodeComponent.compDiameter.validation === 2) {
      computationFail++;
    }
  }

  if ((computationFail / listComponents.length) >= 1 || (computationWarn / listComponents.length) > 2) {
    return 3;
  } else if ((computationWarn / listComponents.length) >= 1) {
    return 2;
  } else {
    return 1;
  }
}

export default {
  Query: {
    getFeatures: builderRandomFeature,
    getFeaturesAutoUp: builderRandomFeatureAuto
  },

  Subscription: {
    changeFeature: {
      subscribe: (_, __, { pubsub }) => pubsub.asyncIterator("changeFeature"),
    },
  }
}
