import graphqlyoga from "graphql-yoga"
const { GraphQLServer, PubSub } = graphqlyoga

const pubsub = new PubSub();

// Main contener of resolvers for the graph queries
import resolvers from './src/resolvers/featureResolvers.js';

// Main contener teh definition os types and queries on Graph queries
import typeDefs from './src/schemas/featuresSchemma.js';

const server = new GraphQLServer({
                      typeDefs,
                      resolvers,
                      context: ({ req }) => ({ req, pubsub})
});
server.start(console.log("gql node server running on local host 4000"))
